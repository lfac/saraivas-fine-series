$(function() {
    $('.oppener').click(function(e){
        var st = $(this).siblings('.visibilityToogleable:hidden');
        if(st.size() > 0){
            st.show();
        }
        else{
            $(this).siblings('.visibilityToogleable:visible').hide(); 
        }
    });
    
    $(".fixUrls a").each(function(i, v){
    	if(this.href.indexOf('?') < 0){
    		var sa = this.href.split('/');
    		this.href = sa[sa.length - 3] + "?id=" + sa[sa.length - 2];
    	}
    });
    
    $(".removeLastComma").each(function(i, v){
    	var ih = jQuery.trim(this.innerHTML);
    	if(ih[ih.length - 1]==','){
    		this.innerHTML = ih.substr(0, ih.length - 1);
    	}
    });
    
    $(".hideIfNotFound").each(function(i, v){
    	if(this.innerHTML.indexOf("NOTFOUND") >= 0){
    		$(this).hide();
    	}
    });
    
    $(".removeEndingCharacterString").each(function(i, v){
    	var tjq = $(v);
    	var tt = tjq.text();
    	var cs_index = tt.indexOf(" (Character)");
    	if(cs_index >= 0){
    		tjq.text(tt.substring(0, cs_index));
    	}
    });
    
    $(".jMyCarousel").jMyCarousel({
        visible: '100%',
        step: 125,
        circular: false
    });
    
    $(".jMyCarousel .recomendedItem").each(function(i, v){
		$(v).hover(
				function(){
					$(this).find(".recomendedItemOverlay").show();
				},
				function(){
					$(this).find(".recomendedItemOverlay").hide();
				}
		);
		
	});
    
    if(window.lastInit){
    	lastInit();
    }
});

$(window).load(function() {
    if($('#slider').size()>0){
        $('#slider').nivoSlider();
    }
    if($('#tabs').size()>0){
        $('#tabs').tabs();
    }
    $('.ui_link').hover(
        function() { $(this).addClass('ui-state-hover'); }, 
        function() { $(this).removeClass('ui-state-hover'); }
    );
        
    $("#searchSwitch .cb-enable").click(function(){
        var parent = $(this).parents('.switch');
        $('#searchSwitch .cb-disable',parent).removeClass('selected');
        $(this).addClass('selected');
        $('.checkbox',parent).attr('checked', true);
    });
    $("#searchSwitch .cb-disable").click(function(){
        var parent = $(this).parents('.switch');
        $('#searchSwitch .cb-enable',parent).removeClass('selected');
        $(this).addClass('selected');
        $('.checkbox',parent).attr('checked', false);
    });
    
    $("#searchLangSwitch .cb-enable").click(function(){
        var parent = $(this).parents('.switch');
        $('#searchLangSwitch .cb-disable',parent).removeClass('selected');
        $(this).addClass('selected');
        $('.checkbox',parent).attr('checked', true);
    });
    $("#searchLangSwitch .cb-disable").click(function(){
        var parent = $(this).parents('.switch');
        $('#searchLangSwitch .cb-enable',parent).removeClass('selected');
        $(this).addClass('selected');
        $('.checkbox',parent).attr('checked', false);
    });
    
    $('.applyBolding p').each(function(i, v){
        var jqv = $(v);
        var sa = jqv.html().split(":");
        if(sa.length == 2 && sa[1][0]!="/"){
            jqv.html('<span class="bold">'+sa[0]+'</span>' + ":" +sa[1]);
        }
    });
});









