package models;

import java.util.ArrayList;
import java.util.Hashtable;

import play.Logger;

import com.hp.hpl.jena.query.QueryExecution;
import com.hp.hpl.jena.query.QueryExecutionFactory;
import com.hp.hpl.jena.query.QueryFactory;
import com.hp.hpl.jena.query.QuerySolution;
import com.hp.hpl.jena.query.ResultSet;
import com.hp.hpl.jena.rdf.model.Model;
import com.hp.hpl.jena.rdf.model.Resource;
import com.hp.hpl.jena.vocabulary.RDF;
import com.hp.hpl.jena.vocabulary.RDFS;

public class Person extends TSModel{
	private String bio, picture, dateOfBirth, name, id, type;
	public static final String ACTOR = "Actor";
	public static final String CHAR = "Char";
	public static final String CREATOR = "Creator";
	
	private static Hashtable<String, String> mappingPersonSeriesProperty;
	static{
		mappingPersonSeriesProperty = new Hashtable<String,String>();
		mappingPersonSeriesProperty.put("Actor", "hasPerformedInTVSeries");
		mappingPersonSeriesProperty.put("Char", "hasAppearedInTVSeries");
		mappingPersonSeriesProperty.put("Creator", "hasCreatedShow");
	}

	private static Hashtable<String, String> mappingNameProperty;
	static{
		mappingNameProperty = new Hashtable<String,String>();
		mappingNameProperty.put("NAME", "hasName");
	}
	
	public Person(String bio, String picture, String dateOfBirth, String name,
			String id, String type) {
		super();
		this.bio = bio;
		this.picture = picture;
		this.dateOfBirth = dateOfBirth;
		this.name = name;
		this.id = id;
		this.type = type;
	}



	public String getBio() {
		return bio;
	}

	public String getPicture() {
		return picture;
	}

	public String getDate_of_birth() {
		return dateOfBirth;
	}

	public String getName() {
		return name;
	}

	public String getId() {
		return id;
	}
	
	/**
	 * Returns the type of this person. Type can have the values:
	 *	- ACTOR
	 * 	- CHAR
	 * 	- CREATOR
	 * 
	 * @return
	 */
	public String getType() {
		return type;
	}
	
	/**
	 * Returns an instance of person that represents the one with id equal to id
	 * 
	 * @param id
	 * @return
	 */
	public static Person get(String id){
		TSModel tsModel = new TSModel();
		boolean personExists = tsModel.model.contains(tsModel.model.getResource(TSModel.PREFIX+id), RDF.type);
		if(personExists == false) {
			Logger.info("There is no person with that id so the function sent a null");
			return new Person("", "", "", "", "", "");
		}
		Resource personResource = tsModel.model.getResource(TSModel.PREFIX+id);
		//personResource.getProperty(RDF.type).toString();
		//Logger.info("AQUIII : "+personResource.getProperty(RDF.type).toString());
		//Logger.info("person Resource "+personResource.toString());
		String dateOfBirth = "NOTFOUND";
		if( (personResource.getProperty(tsModel.model.getProperty(tsModel.PREFIX+"hasDateOfBirth")))!=null){
			dateOfBirth = personResource.getProperty(tsModel.model.getProperty(tsModel.PREFIX+"hasDateOfBirth")).getString();
		}
		
		Person person = new Person(personResource.getProperty(tsModel.model.getProperty(tsModel.PREFIX+"hasBio")).getString(), personResource.getProperty(tsModel.model.getProperty(tsModel.PREFIX+"hasPicture")).getString(), dateOfBirth, personResource.getProperty(tsModel.model.getProperty(tsModel.PREFIX+"hasName")).getString(), personResource.getProperty(tsModel.model.getProperty(tsModel.PREFIX+"hasID")).getString(), personResource.getProperty(tsModel.model.getProperty(tsModel.PREFIX+"hasTypeOfPerson")).getString());
		return person;
	}

	/**
	 * Returns all the persons of type equal to type. Type can have the values:
	 * 	- ACTOR
	 * 	- CHAR
	 * 	- CREATOR
	 * 
	 * @param type
	 * @return A list of Strings, each containing a person's id
	 */
	public static ArrayList<String> getAll(String type){
		ArrayList<String> allPeopleID = new ArrayList<String>();
		String q1 = "PREFIX foaf:   <"+TSModel.PREFIX+"> SELECT ?s WHERE { ?s foaf:hasTypeOfPerson ?g FILTER regex(?g, \""+type+"\") }";
		com.hp.hpl.jena.query.Query query = QueryFactory.create(q1);
		QueryExecution qexec = QueryExecutionFactory.create(query, (new TSModel()).model);
		ResultSet results = qexec.execSelect();
		//Logger.info(":::::111");
		while(results.hasNext()){
			QuerySolution qs=results.next();
			
			Resource seriesResource = qs.getResource("?s");
			//Logger.info(":::::111" + seriesResource.toString());
			allPeopleID.add(seriesResource.toString().replaceAll(TSModel.PREFIX, ""));
		}
		return allPeopleID;
		
	}
	
	public static final String getAllSF_NAME = "NAME";
	public static final String getAllSF_PICTURE = "PICTURE";
	public static final String getAllSF_DATEOFBIRTH = "DATEOFBIRTH";
	
	/**
	 * Returns all the persons of type equal to type. Type can have the values:
	 * 	- ACTOR
	 * 	- CHAR
	 * 	- CREATOR
	 * Second field can have the following values:
	 * 	- NAME
	 * 	- PICTURE
	 * 	- DATEOFBIRTH
	 * 
	 * This method id useful when building listing of all persons.
	 * 
	 * @param type
	 * @param secondField Dictates what field will come on the second element of the pair
	 * @return A list of String pairs, the first element of the pair is always the id and the second depends on secondField
	 */
	public static ArrayList<Pair<String, String>> getAll(String type, String secondField){
		ArrayList<Pair<String, String>> peopleArrayPair = new ArrayList<Pair<String, String>>();
		String q1 = "PREFIX foaf:   <"+TSModel.PREFIX+"> SELECT ?s ?o WHERE { ?s foaf:"+mappingNameProperty.get(secondField)+" ?o . ?s foaf:hasTypeOfPerson ?p FILTER regex(?p, \""+type+"\")}";
		com.hp.hpl.jena.query.Query query = QueryFactory.create(q1);
		QueryExecution qexec = QueryExecutionFactory.create(query, (new TSModel()).model);
		ResultSet results = qexec.execSelect();
		while(results.hasNext()){
			QuerySolution qs=results.next();
			
			Resource seriesResource = qs.getResource("?s");
			peopleArrayPair.add(new Pair<String,String>(seriesResource.toString().replaceAll(TSModel.PREFIX, ""), qs.getLiteral("?o").toString()));
			
			//allSeriesIDs.add(seriesResource.toString().replaceAll(TSModel.PREFIX, ""));
		}
		return peopleArrayPair;
	}
	
	
	public ArrayList<Series> getPersonSeries(){
		ArrayList<Series> seriesAL = new ArrayList<Series>();
		
		Model model = TripleStoreManager.getInstance();
		String q1 = "PREFIX foaf:   <"+TSModel.PREFIX+"> SELECT ?o WHERE { foaf:"+this.id+" foaf:"+mappingPersonSeriesProperty.get(type)+" ?o}";
		com.hp.hpl.jena.query.Query query = QueryFactory.create(q1);
		QueryExecution qexec = QueryExecutionFactory.create(query, model);
		ResultSet results = qexec.execSelect();
		while(results.hasNext()){
			QuerySolution qs=results.next();
			
			Resource seriesResource = qs.getResource("?o");
			Series s = Series.get(seriesResource.toString().replace(TSModel.PREFIX, ""));
			seriesAL.add(s);
			//res.add(genreResource.toString().replace(TSModel.PREFIX, ""));
		}
		
		return seriesAL;
		
	}
}
