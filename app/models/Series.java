package models;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Hashtable;

import play.Logger;

import com.hp.hpl.jena.query.QueryExecution;
import com.hp.hpl.jena.query.QueryExecutionFactory;
import com.hp.hpl.jena.query.QueryFactory;
import com.hp.hpl.jena.query.QuerySolution;
import com.hp.hpl.jena.query.ResultSet;
import com.hp.hpl.jena.rdf.model.ResIterator;
import com.hp.hpl.jena.rdf.model.Resource;
import com.hp.hpl.jena.rdf.model.Statement;
import com.hp.hpl.jena.rdf.model.StmtIterator;

public class Series extends TSModel {
	private String title, runtime, storyline, tagline
	, coutry, language, poster, released, id;
	private ArrayList<Person> creators, stars, actors, chars;
	private ArrayList<String> plotKeywords, genres;
	private String numberOfSeasons, year, numberOfReviews, numberOfRatings, numberOfVotes;
	private String rating;
	private ArrayList<Hashtable<String, String>> reviews; 
	private ArrayList<ArrayList<Hashtable<String, String>>> seriesAndEps;
	private static Hashtable<String, String> mappingNameProperty;
	static{
		mappingNameProperty = new Hashtable<String,String>();
		mappingNameProperty.put("NAME", "hasTVSeriesTitle");
	}
	
	private String age_rating, plot;
	private ArrayList<Pair<Person, Person>> actor2Char;
	
	public Series() {
		super();
		
	}

	public Series(String title, String runtime, String storyline,
			String tagline, String coutry, String language, String poster,
			String released, ArrayList<Person> creators,
			ArrayList<Person> stars, ArrayList<String> plotKeywords,
			String numberOfSeasons, String year, String numberOfReviews,
			String numberOfRatings, String rating,
			ArrayList<Hashtable<String, String>> reviews,
			ArrayList<ArrayList<Hashtable<String, String>>> seriesAndEps) {
		super();
		this.title = title;
		this.runtime = runtime;
		this.storyline = storyline;
		this.tagline = tagline;
		this.coutry = coutry;
		this.language = language;
		this.poster = poster;
		this.released = released;
		this.creators = creators;
		this.stars = stars;
		this.plotKeywords = plotKeywords;
		this.numberOfSeasons = numberOfSeasons;
		this.year = year;
		this.numberOfReviews = numberOfReviews;
		this.numberOfRatings = numberOfRatings;
		this.rating = rating;
		this.reviews = reviews;
		this.seriesAndEps = seriesAndEps;
	}

	
	
	public String getNumberOfVotes() {
		return numberOfVotes;
	}

	public void setNumberOfVotes(String numberOfVotes) {
		this.numberOfVotes = numberOfVotes;
	}

	public ArrayList<String> getGenres() {
		return genres;
	}

	public void setGenres(ArrayList<String> genres) {
		this.genres = genres;
	}

	public String getId() {
		return id;
	}

	public String getTitle() {
		return title;
	}

	public String getRuntime() {
		return runtime;
	}

	public String getStoryline() {
		return storyline;
	}

	public String getTagline() {
		return tagline;
	}

	public String getCoutry() {
		return coutry;
	}

	public String getLanguage() {
		return language;
	}

	public String getPoster() {
		return !poster.equals("NOTFOUND") ? poster : "";
	}

	public String getReleased() {
		return released;
	}

	public ArrayList<String> getPlotKeywords() {
		return plotKeywords;
	}

	public ArrayList<Person> getActors() {
		return actors;
	}

	public void setActors(ArrayList<Person> actors) {
		this.actors = actors;
	}

	public ArrayList<Person> getChars() {
		return chars;
	}

	public void setChars(ArrayList<Person> chars) {
		this.chars = chars;
	}

	public ArrayList<Hashtable<String, String>> getReviews() {
		return reviews;
	}
	
	
	
	public void setTitle(String title) {
		this.title = title;
	}

	public void setRuntime(String runtime) {
		this.runtime = runtime;
	}

	public void setStoryline(String storyline) {
		this.storyline = storyline;
	}

	public void setTagline(String tagline) {
		this.tagline = tagline;
	}

	public void setCoutry(String coutry) {
		this.coutry = coutry;
	}

	public void setLanguage(String language) {
		this.language = language;
	}

	public void setPoster(String poster) {
		this.poster = poster;
	}

	public void setReleased(String released) {
		this.released = released;
	}

	public void setId(String id) {
		this.id = id;
	}

	public void setPlotKeywords(ArrayList<String> plotKeywords) {
		this.plotKeywords = plotKeywords;
	}
	


	public String getNumberOfSeasons() {
		return numberOfSeasons;
	}

	public void setNumberOfSeasons(String numberOfSeasons) {
		this.numberOfSeasons = numberOfSeasons;
	}

	public String getYear() {
		return year;
	}

	public void setYear(String year) {
		this.year = year;
	}

	public String getNumberOfReviews() {
		return numberOfReviews;
	}

	public void setNumberOfReviews(String numberOfReviews) {
		this.numberOfReviews = numberOfReviews;
	}

	public String getNumberOfRatings() {
		return numberOfRatings;
	}

	public void setNumberOfRatings(String numberOfRatings) {
		this.numberOfRatings = numberOfRatings;
	}

	public String getRating() {
		return rating;
	}

	public void setRating(String rating) {
		this.rating = rating;
	}

	public void setReviews(ArrayList<Hashtable<String, String>> reviews) {
		this.reviews = reviews;
	}

	public void setSeriesAndEps(
			ArrayList<ArrayList<Hashtable<String, String>>> seriesAndEps) {
		this.seriesAndEps = seriesAndEps;
	}
	
	

	public ArrayList<Person> getCreators() {
		return creators;
	}

	public void setCreators(ArrayList<Person> creators) {
		this.creators = creators;
	}

	public ArrayList<Person> getStars() {
		return stars;
	}

	public void setStars(ArrayList<Person> stars) {
		this.stars = stars;
	}

	public String getAge_rating() {
		return age_rating;
	}

	public void setAge_rating(String age_rating) {
		this.age_rating = age_rating;
	}

	public String getPlot() {
		return plot;
	}

	public void setPlot(String plot) {
		this.plot = plot;
	}

	public ArrayList<Pair<Person, Person>> getActor2Char() {
		return actor2Char;
	}

	public void setActor2Char(ArrayList<Pair<Person, Person>> actor2Char) {
		this.actor2Char = actor2Char;
	}

	public Hashtable<String, String> getReview(int index){
		//if(numberOfReviews.equals("NOTFOUND")) return new Hashtable<String, String>();
		if(index >= 0 && index < reviews.size()){
			return reviews.get(index);
		}
		else{
			return new Hashtable<String, String>();
		}
	}

	public ArrayList<ArrayList<Hashtable<String, String>>> getSeriesAndEps() {
		return seriesAndEps;
	}
	
	public ArrayList<Hashtable<String, String>> getSeason(int index){
		if(numberOfSeasons.equals("NOTFOUND")) return new ArrayList<Hashtable<String,String>>();
		if(index >= 0 && index < Integer.parseInt(numberOfSeasons)){
			return seriesAndEps.get(index);
		}
		else{
			return new ArrayList<Hashtable<String,String>>();
		}
	}
	
	public int getNumberOfEpisodesInSeason(int seasonIndex){
		return getSeason(seasonIndex).size();
	}
	
	public Hashtable<String, String> getEpisode(int seasonIndex, int episodeIndex){
		if(episodeIndex >= 0 && episodeIndex < getNumberOfEpisodesInSeason(seasonIndex)){
			return getSeason(seasonIndex).get(episodeIndex);
		}
		else{
			return new Hashtable<String, String>();
		}
	}
	
	/**
	 * Returns an instance of series that represents the one with id equal to id
	 * 
	 * @param id
	 * @return
	 */
	public static Series get(String id){
		Series series = new Series();
		
		Resource resourceSeries = series.model.getResource(series.PREFIX + id);
		series.setId(id);
		series.setTitle(resourceSeries.getProperty(series.model.getProperty(series.PREFIX+"hasTVSeriesTitle")).getString());
		series.setRuntime(resourceSeries.getProperty(series.model.getProperty(series.PREFIX+"hasRuntime")).getString());
		series.setStoryline(resourceSeries.getProperty(series.model.getProperty(series.PREFIX+"hasStoryline")).getString());
		series.setTagline(resourceSeries.getProperty(series.model.getProperty(series.PREFIX+"hasTagline")).getString());
		series.setCoutry(resourceSeries.getProperty(series.model.getProperty(series.PREFIX+"hasCountry")).getString());
		series.setLanguage(resourceSeries.getProperty(series.model.getProperty(series.PREFIX+"hasLanguage")).getString());
		series.setPoster(resourceSeries.getProperty(series.model.getProperty(series.PREFIX+"hasPoster")).getString());
		series.setReleased(resourceSeries.getProperty(series.model.getProperty(series.PREFIX+"hasReleasedDate")).getString());
		series.setNumberOfSeasons(resourceSeries.getProperty(series.model.getProperty(series.PREFIX+"hasNumberOfSeasons")).getString());
		series.setYear(resourceSeries.getProperty(series.model.getProperty(series.PREFIX+"hasYear")).getString());
		series.setNumberOfReviews(resourceSeries.getProperty(series.model.getProperty(series.PREFIX+"hasNumberOfReviews")).getString());
		series.setNumberOfVotes(resourceSeries.getProperty(series.model.getProperty(series.PREFIX+"hasNumberOfVotes")).getString());
		series.setNumberOfRatings(resourceSeries.getProperty(series.model.getProperty(series.PREFIX+"hasNumberOfRatings")).getString());
		series.setPlot(resourceSeries.getProperty(series.model.getProperty(series.PREFIX+"hasPlot")).getString());
		series.setRating(resourceSeries.getProperty(series.model.getProperty(series.PREFIX+"hasRating")).getString());
		
		// Get Creators
		String q1 = "PREFIX foaf:   <"+series.PREFIX+"> SELECT ?o WHERE { foaf:"+id+" foaf:isCreatedByCreator ?o}";
		com.hp.hpl.jena.query.Query query = QueryFactory.create(q1);
		QueryExecution qexec = QueryExecutionFactory.create(query, series.model);
		ResultSet results = qexec.execSelect();
		series.setCreators(new ArrayList<Person>());
		while(results.hasNext()){
			QuerySolution qs=results.next();
			
			Resource creatorResource = qs.getResource("?o");
			Person person = new Person(creatorResource.getProperty(series.model.getProperty(series.PREFIX+"hasBio")).getString(), creatorResource.getProperty(series.model.getProperty(series.PREFIX+"hasPicture")).getString(), creatorResource.getProperty(series.model.getProperty(series.PREFIX+"hasDateOfBirth")).getString(), creatorResource.getProperty(series.model.getProperty(series.PREFIX+"hasName")).getString(), creatorResource.getProperty(series.model.getProperty(series.PREFIX+"hasID")).getString(), creatorResource.getProperty(series.model.getProperty(series.PREFIX+"hasTypeOfPerson")).getString());
			series.getCreators().add(person);
		}
		// Get Stars
		String q2 = "PREFIX foaf:   <"+series.PREFIX+"> SELECT ?o WHERE { foaf:"+id+" foaf:hasStar ?o}";
		query = QueryFactory.create(q2);
		qexec = QueryExecutionFactory.create(query, series.model);
		results = qexec.execSelect();
		series.setStars(new ArrayList<Person>());
		while(results.hasNext()){
			QuerySolution qs=results.next();
			
			Resource creatorResource = qs.getResource("?o");
			Person person = Person.get(creatorResource.toString().replace(TSModel.PREFIX, ""));
			//Person person = new Person(creatorResource.getProperty(series.model.getProperty(series.PREFIX+"hasBio")).getString(), creatorResource.getProperty(series.model.getProperty(series.PREFIX+"hasPicture")).getString(), creatorResource.getProperty(series.model.getProperty(series.PREFIX+"hasDateOfBirth")).getString(), creatorResource.getProperty(series.model.getProperty(series.PREFIX+"hasName")).getString(), creatorResource.getProperty(series.model.getProperty(series.PREFIX+"hasID")).getString(), creatorResource.getProperty(series.model.getProperty(series.PREFIX+"hasTypeOfPerson")).getString());
			series.getStars().add(person);
		}
		
		// Get Actors
		String q3 = "PREFIX foaf:   <"+series.PREFIX+"> SELECT ?o WHERE { foaf:"+id+" foaf:hasTVSeriesActor ?o}";
		query = QueryFactory.create(q3);
		qexec = QueryExecutionFactory.create(query, series.model);
		results = qexec.execSelect();
		series.setActors(new ArrayList<Person>());
		while(results.hasNext()){
			QuerySolution qs=results.next();
			
			Resource creatorResource = qs.getResource("?o");
			Person person = new Person(creatorResource.getProperty(series.model.getProperty(series.PREFIX+"hasBio")).getString(), creatorResource.getProperty(series.model.getProperty(series.PREFIX+"hasPicture")).getString(), creatorResource.getProperty(series.model.getProperty(series.PREFIX+"hasDateOfBirth")).getString(), creatorResource.getProperty(series.model.getProperty(series.PREFIX+"hasName")).getString(), creatorResource.getProperty(series.model.getProperty(series.PREFIX+"hasID")).getString(), creatorResource.getProperty(series.model.getProperty(series.PREFIX+"hasTypeOfPerson")).getString());
			series.getActors().add(person);
		}
		
		// Get Chars
		String q4 = "PREFIX foaf:   <"+series.PREFIX+"> SELECT ?o WHERE { foaf:"+id+" foaf:hasTVSeriesCharacter ?o}";
		query = QueryFactory.create(q4);
		qexec = QueryExecutionFactory.create(query, series.model);
		results = qexec.execSelect();
		series.setChars(new ArrayList<Person>());
		series.setActor2Char(new ArrayList<Pair<Person, Person>>());
		while(results.hasNext()){
			QuerySolution qs=results.next();
			//System.out.println(qs.toString());
			Resource creatorResource = qs.getResource("?o");
			if(creatorResource==null){
				creatorResource.getProperty(series.model.getProperty(series.PREFIX+"hasBio")).getString();
			}
			//String bio = creatorResource.getProperty(series.model.getProperty(series.PREFIX+"hasBio")).getString();
			//String picture = creatorResource.getProperty(series.model.getProperty(series.PREFIX+"hasPicture")).getString();
			//String name = creatorResource.getProperty(series.model.getProperty(series.PREFIX+"hasName")).getString();
			//String charID = creatorResource.getProperty(series.model.getProperty(series.PREFIX+"hasID")).getString();
			//Person person = new Person("", "", "NOTFOUND", creatorResource.getProperty(series.model.getProperty(series.PREFIX+"hasName")).getString(), creatorResource.getProperty(series.model.getProperty(series.PREFIX+"hasID")).getString(), Person.CHAR);
			
			Person person = new Person(creatorResource.getProperty(series.model.getProperty(series.PREFIX+"hasBio")).getString(), creatorResource.getProperty(series.model.getProperty(series.PREFIX+"hasPicture")).getString(), "NOTFOUND", creatorResource.getProperty(series.model.getProperty(series.PREFIX+"hasName")).getString(), creatorResource.getProperty(series.model.getProperty(series.PREFIX+"hasID")).getString(), creatorResource.getProperty(series.model.getProperty(series.PREFIX+"hasTypeOfPerson")).getString());
			
			Pair<Person, Person> e = new Pair<Person, Person>(Utils.getActorFromChar(person.getId()), person);
			series.getActor2Char().add(e);
			series.getChars().add(person);
		}
		
		
		//Get keywords
		Resource seriesRes = series.model.getResource(series.PREFIX + id);
		StmtIterator iter = seriesRes.listProperties(series.model.getProperty(series.PREFIX + "hasPlotKeyword"));
		series.setPlotKeywords(new ArrayList<String>());
		while(iter.hasNext()){
			series.getPlotKeywords().add(iter.nextStatement().getObject().toString());
		}
		

		
		// Reviewss
		series.setReviews(new ArrayList<Hashtable<String, String>>());
		
		String q5 = "PREFIX foaf:   <"+series.PREFIX+"> SELECT ?o WHERE { foaf:"+id+" foaf:hasReview ?o}";
		query = QueryFactory.create(q5);
		qexec = QueryExecutionFactory.create(query, series.model);
		results = qexec.execSelect();
		while(results.hasNext()){
			QuerySolution qs=results.next();
			
			Resource reviewResource = qs.getResource("?o");
			Hashtable<String, String> reviewHash = new Hashtable<String, String>();
			reviewHash.put("Date", reviewResource.getProperty(series.model.getProperty(series.PREFIX+"hasDate")).getString());
			reviewHash.put("ReviewRating", reviewResource.getProperty(series.model.getProperty(series.PREFIX+"hasReviewRating")).getString());
			reviewHash.put("ReviewTitle", reviewResource.getProperty(series.model.getProperty(series.PREFIX+"hasReviewTitle")).getString());
			reviewHash.put("TextReview", reviewResource.getProperty(series.model.getProperty(series.PREFIX+"hasTextReview")).getString());
			reviewHash.put("PeopleFoundUsefull", reviewResource.getProperty(series.model.getProperty(series.PREFIX+"hasPeopleFoundUsefull")).getString());
			//hasPeopleFoundUsefull
			
			String q20 = "PREFIX foaf:   <"+series.PREFIX+"> SELECT ?o WHERE { foaf:"+reviewResource.toString().replace(series.PREFIX, "")+" foaf:isWrittenBy ?o}";
			com.hp.hpl.jena.query.Query queryrev = QueryFactory.create(q20);
			QueryExecution qexecrev = QueryExecutionFactory.create(queryrev, series.model);
			ResultSet resultsrev = qexecrev.execSelect();
			if(resultsrev.hasNext()){
				QuerySolution qs1=resultsrev.next();
				
				Resource reviewerResource = qs1.getResource("?o");
				reviewHash.put("WrittenBy", reviewerResource.getProperty(series.model.getProperty(series.PREFIX+"hasName")).getString());
				reviewHash.put("WrittenByID", reviewerResource.toString().replace(series.PREFIX, ""));
				reviewHash.put("Localization", reviewerResource.getProperty(series.model.getProperty(series.PREFIX+"hasLocalization")).getString());
			}
			
			series.getReviews().add(reviewHash);
		}
		
		// Seasons and Episodes
		series.setSeriesAndEps(new ArrayList<ArrayList<Hashtable<String, String>>>());
		String q6 = "PREFIX foaf:   <"+series.PREFIX+"> SELECT ?o WHERE { foaf:"+id+" foaf:hasSeason ?o . ?o foaf:hasSeasonNumber ?snumber} ORDER BY ?snumber";
		//System.out.println("q6 "+q6);
		query = QueryFactory.create(q6);
		qexec = QueryExecutionFactory.create(query, series.model);
		results = qexec.execSelect();
		
		while(results.hasNext()){
			QuerySolution qs=results.next();
			
			Resource seasonResource = qs.getResource("?o");
			ArrayList<Hashtable<String, String>> seasonsArray = new ArrayList<Hashtable<String, String>>();
			
			//System.out.println("MEEEEE");
			//String q7 = "PREFIX foaf:   <"+series.PREFIX+"> SELECT ?o WHERE { foaf:"+seasonResource.toString().replaceAll(series.PREFIX, "")+" foaf:hasEpisode ?o . ?o foaf:hasEpisodeNumber ?enumber} ORDER BY ?enumber";
			String q7 = "PREFIX foaf:   <"+series.PREFIX+"> SELECT ?o WHERE { foaf:"+seasonResource.toString().replaceAll(series.PREFIX, "")+" foaf:hasEpisode ?o . ?o foaf:hasEpisodeNumber ?enumber} ORDER BY ?enumber";
			//System.out.println("q7 "+q7);
			
			//Logger.info("string "+seasonResource.toString());
			com.hp.hpl.jena.query.Query queryepi = QueryFactory.create(q7);
			QueryExecution qexecepi = QueryExecutionFactory.create(queryepi, series.model);
			ResultSet resultsepi = qexecepi.execSelect();
			while(resultsepi.hasNext()){
				QuerySolution qs1=resultsepi.next();
				
				Resource epResource = qs1.getResource("?o");
				//Logger.info("string "+epResource.toString());
				Hashtable<String, String> episodeHash = new Hashtable<String, String>();
				episodeHash.put("AirDate", epResource.getProperty(series.model.getProperty(series.PREFIX+"hasAirDate")).getString());
				episodeHash.put("EpisodeStoryline", epResource.getProperty(series.model.getProperty(series.PREFIX+"hasEpisodeStoryline")).getString());
				episodeHash.put("EpisodeTitle", epResource.getProperty(series.model.getProperty(series.PREFIX+"hasEpisodeTitle")).getString());
				seasonsArray.add(episodeHash);
				
				//System.out.println("_____Air Date "+epResource.getProperty(series.model.getProperty(series.PREFIX+"hasAirDate")).getString()+ " episode title "+ epResource.getProperty(series.model.getProperty(series.PREFIX+"hasEpisodeTitle")).getString() + " number "+epResource.getProperty(series.model.getProperty(series.PREFIX+"hasEpisodeNumber")).getString());
				//ArrayList<Hashtable<String, String>> seasonsArray = new ArrayList<Hashtable<String, String>>();
				
			}
			//System.out.println("MEEEEE");
			series.getSeriesAndEps().add(seasonsArray);
			
		}
		//System.out.println("me "+series.getSeriesAndEps().size());
		
		
		// Get genres
		
		String q8 = "PREFIX foaf:   <"+series.PREFIX+"> SELECT ?o WHERE { foaf:"+id+" foaf:hasGenre ?o}";
		query = QueryFactory.create(q8);
		qexec = QueryExecutionFactory.create(query, series.model);
		results = qexec.execSelect();
		series.setGenres(new ArrayList<String>());
		while(results.hasNext()){
			QuerySolution qs=results.next();
			
			Resource genreResource = qs.getResource("?o");
			series.getGenres().add(genreResource.toString().replace(series.PREFIX, "").trim());
		}
		series.setAge_rating(resourceSeries.getProperty(series.model.getProperty(series.PREFIX+"isRated")).getString());
		return series;
		
		
	}

	/**
	 * Returns all the series.
	 * 
	 * @param type
	 * @return A list of Strings, each containing a series' id
	 */
	public static ArrayList<String> getAll(){
		// Get Series
		ArrayList<String> allSeriesIDs = new ArrayList<String>();
		String q1 = "PREFIX foaf:   <"+TSModel.PREFIX+"> SELECT ?s WHERE { ?s foaf:hasTVSeriesTitle ?o}";
		com.hp.hpl.jena.query.Query query = QueryFactory.create(q1);
		QueryExecution qexec = QueryExecutionFactory.create(query, (new TSModel()).model);
		ResultSet results = qexec.execSelect();
		while(results.hasNext()){
			QuerySolution qs=results.next();
			
			Resource seriesResource = qs.getResource("?s");
			//Logger.info(":::::" + seriesResource.toString());
			allSeriesIDs.add(seriesResource.toString().replaceAll(TSModel.PREFIX, ""));
		}
		return allSeriesIDs;
	}
	
	public static final String getAllSF_NAME = "NAME";
	
	//TODO: Pensar que outros parametros é que pode dar jeito tirar com isto
	/**
	 * Returns all the series.
	 * Second field can have the following values:
	 * 	- NAME
	 * 
	 * This method id useful when building listing of all series.
	 * 
	 * @param secondField Dictates what field will come on the second element of the pair
	 * @return A list of String pairs, the first element of the pair is always the id and the second depends on secondField
	 */
	public static ArrayList<Pair<String, String>> getAll(String secondField){
		ArrayList<Pair<String, String>> seriesArrayPair = new ArrayList<Pair<String, String>>();
		String q1 = "PREFIX foaf:   <"+TSModel.PREFIX+"> SELECT ?s ?o WHERE { ?s foaf:"+mappingNameProperty.get(secondField)+" ?o}";
		com.hp.hpl.jena.query.Query query = QueryFactory.create(q1);
		QueryExecution qexec = QueryExecutionFactory.create(query, (new TSModel()).model);
		ResultSet results = qexec.execSelect();
		while(results.hasNext()){
			QuerySolution qs=results.next();
			
			Resource seriesResource = qs.getResource("?s");
			seriesArrayPair.add(new Pair<String,String>(seriesResource.toString().replaceAll(TSModel.PREFIX, ""), qs.getLiteral("?o").toString()));
			
			//allSeriesIDs.add(seriesResource.toString().replaceAll(TSModel.PREFIX, ""));
		}
		return seriesArrayPair;
	}
	
	public static ArrayList<Series> getAllGoodTVSeries(){
		ArrayList<Series> goodSeries = new ArrayList<Series>();
		String q1 = "PREFIX foaf:   <"+TSModel.PREFIX+"> PREFIX foaf2: <"+TSModel.TYPEPREFIX+"> SELECT ?s WHERE { ?s foaf2:type foaf:GoodTVSeries}";
		com.hp.hpl.jena.query.Query query = QueryFactory.create(q1);
		QueryExecution qexec = QueryExecutionFactory.create(query, (new TSModel()).model);
		ResultSet results = qexec.execSelect();
		while(results.hasNext()){
			QuerySolution qs=results.next();
			
			Resource seriesResource = qs.getResource("?s");
			goodSeries.add(Series.get(seriesResource.toString().replaceAll(TSModel.PREFIX, "")));
		}
		return goodSeries;
		
	}
	
	public static ArrayList<Series> getAllInterestingTVSeries(){
		ArrayList<Series> interestinS = new ArrayList<Series>();
		String q1 = "PREFIX foaf:   <"+TSModel.PREFIX+"> PREFIX foaf2: <"+TSModel.TYPEPREFIX+"> SELECT ?s WHERE { ?s foaf2:type foaf:InterestingTVSeries}";
		com.hp.hpl.jena.query.Query query = QueryFactory.create(q1);
		QueryExecution qexec = QueryExecutionFactory.create(query, (new TSModel()).model);
		ResultSet results = qexec.execSelect();
		while(results.hasNext()){
			QuerySolution qs=results.next();
			
			Resource seriesResource = qs.getResource("?s");
			interestinS.add(Series.get(seriesResource.toString().replaceAll(TSModel.PREFIX, "")));
		}
		return interestinS;
		
	}
	
	public boolean isInteresting(){
		if((numberOfReviews.equals("NOTFOUND")==false) && (numberOfVotes.equals("NOTFOUND")==false)){
			if((Integer.parseInt(numberOfReviews)>100) && (Integer.parseInt(numberOfVotes)>30000)){
				return true;
			}
		}
		return false;
	}
	
	public boolean isGood(){
		if(rating.equals("NOTFOUND")==false){
			if(Float.parseFloat(rating)>8){
				return true;
			}
		}
		return false;
	}
	
	public boolean equals(Object s){
		if(s instanceof Series)
			return getId().equals(((Series)s).getId());
		else
			return false;
	}
	
	public static ArrayList<Series> getNRecomended4Anyone(int n, String excludeThisId){
		System.out.println("[getNRecomended4Anyone]");
		ArrayList<Series> res = Series.getAllGoodTVSeries();
		for(int i=0; i < res.size(); i++){
			Series sr = res.get(i);
			if(sr.getId().equals(excludeThisId)){
				res.remove(i);
				i--;
			}
		}
		
		for(Series sr : Series.getAllInterestingTVSeries()){
			if(!res.contains(sr) && !sr.getId().equals(excludeThisId))
				res.add(sr);
		}
		Collections.shuffle(res);
		
		if(n > res.size())
			n = res.size();
		
		return new ArrayList<Series>(res.subList(0, n));
	}
}
