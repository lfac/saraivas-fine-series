package models;

import java.util.ArrayList;

public class JSONAssemler {
	public static String toJSONMedium(ArrayList<Series> al){
		StringBuilder res = new StringBuilder("{");
		boolean isFirst = true;
		
		for(Series s : al){
			if(isFirst)
				isFirst = false;
			else
				res.append(",");
			
			StringBuilder cObj = new StringBuilder("\"");
			cObj.append(s.getId()).append("\" : {");
			
			cObj.append("\"title\" : \"").append(s.getTitle()).append("\"");
			cObj.append(", \"img\" : \"").append(s.getPoster()).append("\"");
			cObj.append(", \"rating\" : \"").append(s.getRating()).append("\"");
			cObj.append(", \"year\" : \"").append(s.getYear()).append("\"");
			cObj.append(", \"genres\" : [");
			boolean isFirstGenre = true;
			for(String gstr : s.getGenres()){
				if(!isFirstGenre){
					cObj.append(",");
				}
				else
					isFirstGenre = false;
				cObj.append("\"").append(gstr).append("\"");
			}
			cObj.append("]");
			
			cObj.append("}");
			res.append(cObj);
		}
		
		res.append("}");
		return res.toString();
	}
	
	public static String toJSONLite(ArrayList<Pair<Series, Double>> al){
		StringBuilder res = new StringBuilder("[");
		boolean isFirst = true;
		
		for(Pair<Series, Double> pa : al){
			if(isFirst)
				isFirst = false;
			else
				res.append(",");
			
			Series s = pa.t;
			StringBuilder cObj = new StringBuilder("{");
			cObj.append("id : '").append(s.getId()).append("'");
			cObj.append(", rank : '").append(pa.k).append("'");
			
			cObj.append("}");
			res.append(cObj);
		}
		
		res.append("]");
		return res.toString();
	}
}
