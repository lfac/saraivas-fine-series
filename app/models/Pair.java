package models;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;

public class Pair<T, K> implements Comparable<Pair<T, K>>, Serializable {
	public T t;
	public K k;

	public Pair(T t, K k) {
		this.t = t;
		this.k = k;
	}

	public boolean equals(Object o) {
		if (!(o instanceof Pair))
			return false;
		Pair p = (Pair) o;
		return p.t.equals(t) && p.k.equals(k);
	}

	public String toString() {
		return "<" + t + ", " + k + ">";
	}

	public int compareTo(Pair<T, K> o) {
		if (k instanceof String) {
			String my_str = (String) k;
			String your_str = (String) o.k;
			return my_str.compareTo(your_str);
		} else if (k instanceof Integer) {
			return ((Integer) k).compareTo((Integer) o.k);
		} else if (k instanceof Double) {
			return ((Double) k).compareTo((Double) o.k);
		}
		return 0;
	}
	
}
