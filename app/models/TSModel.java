package models;

import com.hp.hpl.jena.query.Dataset;
import com.hp.hpl.jena.query.QueryExecution;
import com.hp.hpl.jena.query.QueryExecutionFactory;
import com.hp.hpl.jena.query.QueryFactory;
import com.hp.hpl.jena.query.QuerySolution;
import com.hp.hpl.jena.query.ResultSet;
import com.hp.hpl.jena.rdf.model.Model;
import com.hp.hpl.jena.tdb.TDBFactory;

public class TSModel {
	
	protected Model model;
	public static final String PREFIX = "http://www.semanticweb.org/ontologies/2011/9/WSTV.owl#";
	public static final String SUBCLASSPREFIX = "http://www.w3.org/2000/01/rdf-schema#";
	public static final String TYPEPREFIX = "http://www.w3.org/1999/02/22-rdf-syntax-ns#";
	
	
	public TSModel(){
		model = TripleStoreManager.getInstance();
	}
	
	public String test(){
		String q = "select * where {?s ?p ?o} limit 10";
		String result="";
    	QueryExecution qexec = QueryExecutionFactory.create(q, model);
		ResultSet results = qexec.execSelect();
		while(results.hasNext()){
			QuerySolution qs=results.next();
			result += "/n " +qs.toString();
			System.out.println(qs.toString());
			//qs.getLiteral(arg0)
		}
		
		return result;
	}
	
	
	

}
