package models;

import com.hp.hpl.jena.query.Dataset;
import com.hp.hpl.jena.rdf.model.Model;
import com.hp.hpl.jena.tdb.TDBFactory;

public class TripleStoreManager {
	protected Model model;
	
	private TripleStoreManager(){
		String directory = "./tdb";
		Dataset dataset = TDBFactory.createDataset(directory);
		this.model = dataset.getDefaultModel();
		//model.close();
	}
	
	public Model getModel() {
		return model;
	}
	
	private static TripleStoreManager instance=null;
	
	public static Model getInstance(){
		if(instance==null){
			instance = new TripleStoreManager();
		}
		return instance.getModel();
	}

}
