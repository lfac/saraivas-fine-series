package models;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.Set;
import java.util.TreeSet;

import play.Logger;

public class SearchModel {
	protected String originalQuery;
	
	public static final String SR_NAME = "Actors and Creators";
	public static final String SR_CHAR = "Characters";
	public static final String SR_TITLE = "Series";
	public static final String SR_EMPTY = " ";
	
	public SearchModel(String query) {
		super();
		this.originalQuery = query;
	}

	public String getOriginalQuery() {
		return originalQuery;
	}

	protected ArrayList<SearchResult> execute(){
		return new ArrayList<SearchResult>();
	}
	
	public Hashtable<String, ArrayList<SearchResult>> getResult(){
		Hashtable<String, ArrayList<SearchResult>> res = new Hashtable<String, ArrayList<SearchResult>>();
		
		//For now put every thing in the root group
		ArrayList<SearchResult> sres = execute();
		TreeSet<SearchResult> resultSet= new TreeSet<SearchResult>();
		resultSet.addAll(sres);
		sres = new ArrayList<SearchResult>();
		sres.addAll(resultSet);
		
		for(SearchResult sr : sres){
			String href = sr.getPageHref();
			
			String sr_key = null;
			
			if(href.contains("name?id=")){
				sr_key = SR_NAME;
			}
			else if(href.contains("character?id=")){
				if(href.contains("character?id=dummy")){
					continue;
				}
				sr_key = SR_CHAR;
			}
			else if(href.contains("title?id=")){
				sr_key = SR_TITLE;
			}
			else{
				sr_key = SR_EMPTY;
			}
			
			
			if(!res.containsKey(sr_key)){
				res.put(sr_key, new ArrayList<SearchResult>());
			}
			
			res.get(sr_key).add(sr);
		}
		
		//res.put(" ", sres);
		
		return res;
	}
}
