package models;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Vector;

import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.index.CorruptIndexException;
import org.apache.lucene.queryParser.ParseException;
import org.apache.lucene.queryParser.QueryParser;
import org.apache.lucene.search.Hits;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.Query;

import play.Logger;

public class SyntacticSearch extends SearchModel {
	private ArrayList<SearchResult> res;
	

	public SyntacticSearch(String query_text) {
		super(query_text);
		res = new ArrayList<SearchResult>();
		//Logger.info("INSIDE SYN");
		boolean error = false; // used to control flow for error messages
		String indexName = "lucene_index"; // local copy of the
												// configuration variable
		IndexSearcher searcher = null; // the searcher used to open/search the
										// index
		Query query = null; // the Query created by the QueryParser
		Hits hits = null; // the search results
		int startindex = 0; // the first index displayed on this page
		int maxpage = 50; // the maximum items displayed on this page
		String queryString = null; // the query entered in the previous page
		String startVal = null; // string version of startindex
		String maxresults = null; // string version of maxpage
		int thispage = 0;

		try {
			searcher = new IndexSearcher(indexName); // create an indexSearcher
														// for our page
														// NOTE: this operation
														// is slow for large
														// indices (much slower
														// than the search
														// itself)
														// so you might want to
														// keep an IndexSearcher
														// open

		} catch (Exception e) {
			Logger.info("PUM 1"+e.toString());
			error = true;
		}

		if (error == false) { // did we open the index?
			queryString = query_text; // get the search criteria
			startVal = "0"; // get the start index
			maxresults = "20"; // get max results per page
			try {
				maxpage = Integer.parseInt(maxresults); // parse the max results
														// first
				startindex = Integer.parseInt(startVal); // then the start index
			} catch (Exception e) {
				Logger.info("PUM 2");
			} // we don't care if something happens we'll just start at 0
				// or end at 50

			if (queryString == null)
				return;

			Analyzer analyzer = new StandardAnalyzer(); // construct our usual
														// analyzer
			try {
				QueryParser qp = new QueryParser("contents", analyzer);
				query = qp.parse(queryString); // parse the
			} catch (ParseException e) {
				error = true;
				Logger.info("PUM 3");
			}
		}

		if (error == false && searcher != null) { // if we've had no errors
			// searcher != null was to handle
			// a weird compilation bug
			thispage = maxpage; // default last element to maxpage
			try {
				hits = searcher.search(query);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				Logger.info("PUM 4");
			} // run the query
			if (hits.length() == 0) { // if we got no results tell the user

				error = true; // don't bother with the rest of the
				// page
			}
		}

		int max_results = hits.length() < 20 ? hits.length() : 20;
		
		if (error == false && searcher != null) {
			for (int i = 0; i < max_results; i++) {
				Document doc;
				try {
					doc = hits.doc(i);
					// get the next document
					//Logger.info(doc.get("title"));
					String doctitle = doc.get("title"); // get its title
					String url = doc.get("path"); // get its path field
					res.add(new SearchResult(url, doctitle));
				} catch (CorruptIndexException e) {
					// TODO Auto-generated catch block
					Logger.info("PUM 5");
					e.printStackTrace();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					Logger.info("PUM 6");
					e.printStackTrace();
				}
			}
		}
	}
	
	protected ArrayList<SearchResult> execute(){
		return res;
	}

}
