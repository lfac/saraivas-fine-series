package models;

import java.util.ArrayList;

public class SearchResult implements Comparable<SearchResult>{
	private String pageHref, pageTitle, group, displayName;//Results with the same group appear in the same section
	private int relevance;//Has to do with the order of the results
	
	public SearchResult(String pageHref){
		super();
		this.pageHref = pageHref;
		this.group = "ROOT";
		this.relevance = 0;
		this.pageTitle = null;
		displayName = pageHref;
	}
	
	public SearchResult(String pageHref, String pageTitle){
		super();
		this.pageHref = pageHref;
		this.group = "ROOT";
		this.relevance = 0;
		this.pageTitle = pageTitle;
		displayName = pageTitle;
	}
	
	public SearchResult(String pageHref, String group, int relevance) {
		super();
		this.pageHref = pageHref;
		this.group = group;
		this.relevance = relevance;
		displayName = pageHref;
	}
	
	public String getDisplayName(){
		return displayName;
	}

	public String getPageHref() {
		return pageHref;
	}

	public String getGroup() {
		return group;
	}

	public int getRelevance() {
		return relevance;
	}

	public String getPageTitle() {
		return pageTitle;
	}

	public static ArrayList<SearchResult> intersect(ArrayList<SearchResult> a1, ArrayList<SearchResult> a2){
		ArrayList<SearchResult> res = new ArrayList<SearchResult>();
		
		for(SearchResult sr : a1){
			if(a2.contains(sr))
				res.add(sr);
		}
		
		return res;
	}
	
	public Series toSeries(){
		int ind = pageHref.indexOf("title?id=");
		if(ind >= 0){
			String id = pageHref.substring(ind + "title?id=".length());
			return Series.get(id);
		}
		else
			return null;
	}
	
	public static ArrayList<Series> toSeries(ArrayList<SearchResult> al){
		ArrayList<Series> res = new ArrayList<Series>();
		
		for(SearchResult sr : al){
			res.add(sr.toSeries());
		}
		
		return res;
	}
	
	public boolean equals(Object obj){
		return getPageHref().equals(((SearchResult)obj).getPageHref());
	}

	@Override
	public int compareTo(SearchResult arg0) {
		if(getPageHref().equals(arg0.getPageHref()))
			return 0;
		else
			return getDisplayName().compareTo(arg0.getDisplayName());
	}
}
