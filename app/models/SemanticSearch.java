package models;

import java.util.ArrayList;

import play.Logger;

import com.hp.hpl.jena.query.QueryExecution;
import com.hp.hpl.jena.query.QueryExecutionFactory;
import com.hp.hpl.jena.query.QueryFactory;
import com.hp.hpl.jena.query.QuerySolution;
import com.hp.hpl.jena.query.ResultSet;
import com.hp.hpl.jena.rdf.model.Model;
import com.hp.hpl.jena.rdf.model.Resource;
import com.hp.hpl.jena.vocabulary.RDF;
import com.hp.hpl.jena.vocabulary.RDFS;

public class SemanticSearch extends SearchModel{
	private String [] words;
	private int words_current_index, words_len;
	private String currentWord, lang;
	
	public SemanticSearch(String query, String lang) {
		super(query);
		
		this.lang = lang;
		words = query.split(" ");
		words_len = words.length;
		go2FirstWord();
	}
	
	/*Word set helper methods - START*/
	private void go2FirstWord(){
		currentWord = words[0];
		words_current_index = 0;
	}
	
	private String nextWord(){
		words_current_index++;
		return currentWord = words[words_current_index];
	}
	
	private int nWordsLeft(){
		return words_len - words_current_index - 1;
	}
	
	private String peekAtNextWord(){
		return words[words_current_index + 1];
	}
	
	private boolean hasNextWord(){
		return words_current_index < (words_len - 1);
	}
	
	private boolean isLast(){
		return words_current_index == (words_len - 1);
	}
	
	private boolean isValidWord(){
		return words_current_index < (words_len);
	}
	
	private String lastWord(){
		return words[words_len - 1];
	}
	
	private String next2End(){
		return next2End(words_len - 1);
	}

	private String next2End(int until){
		StringBuilder res = new StringBuilder();
		
		for(int i=words_current_index+1; i <= until; i++){
			res.append(words[i]);
			
			if(i < until){
				res.append(' ');
			}
		}
		
		return res.toString();
	}
	
	/*Word set helper methods - END*/
	
	public static boolean isInterceptionSeparator(String s){
		return s.toLowerCase().equals("and");
	}
	
	public ArrayList<SearchResult> execute(){
		String classWord, assocClassName;
		
		if((classWord = SemanticSearchUtils.try2Resolve2Class(currentWord))==null){
			return SemanticSearchUtils.find(originalQuery);
		}
		else if(words_len == 1){
			return SemanticSearchUtils.find(classWord, "");
		}
		else if(words_len > 1 && (assocClassName = SemanticSearchUtils.try2Resolve2Class(lastWord()))!=null){
			return SemanticSearchUtils.find(classWord, next2End(words_len - 2), assocClassName);
		}
		else{
			ArrayList<SearchResult> res = new ArrayList<SearchResult>();
			ArrayList<SearchResult> tres;
			boolean isFirstIntersect = true;
			
			while(hasNextWord()){
				if(!isInterceptionSeparator(currentWord)){
					classWord = SemanticSearchUtils.try2Resolve2Class(currentWord);
				}
				
				String instanceStr = "";
				String prospectPropName = SemanticSearchUtils.try2Resolve2Prop(classWord, peekAtNextWord(), lang);
				String prospectRangePropName = SemanticSearchUtils.try2Resolve2RangeProposition(classWord, peekAtNextWord());
				
				if(prospectPropName != null || prospectRangePropName != null){
					nextWord();
				}
				
				if(prospectRangePropName==null){
					while(true){
						nextWord();
	
						if(isInterceptionSeparator(currentWord)){
							break;
						}
						else if(SemanticSearchUtils.try2Resolve2Class(currentWord)==null){
							instanceStr += currentWord + " ";
						}
						else{
							break;
						}
						
						if(!hasNextWord()){
							break;
						}
					}
				}
				
				if(prospectRangePropName != null && nWordsLeft() >= 4 && peekAtNextWord().toLowerCase().equals("from")){
					try{
						nextWord();
						nextWord();
						double fromVal = Double.parseDouble(currentWord);
						nextWord();
						nextWord();
						double toVal = Double.parseDouble(currentWord);
						if(hasNextWord())
							nextWord();
						
						tres = SemanticSearchUtils.findByRange(classWord, prospectRangePropName, fromVal, toVal);
					}catch(NumberFormatException e){
						Logger.warn("The number format in the range query is wrong! Query: "+originalQuery);
						tres = new ArrayList<SearchResult>();
						break;
					}
				}
				//If the word to the right of the class is a propriety do find by prop
				else if(prospectPropName != null){
					System.out.println("BY PROP");
					tres = SemanticSearchUtils.findByProp(classWord, prospectPropName, instanceStr.trim(), lang);
				}
				else{//else consider everything until the next class like part of the instance
					tres = SemanticSearchUtils.find(classWord, instanceStr.trim());
				}
				
				//Executes the intersection of the partial results
				if(res.size() > 0){
					res = SearchResult.intersect(res, tres);
				}
				else if(isFirstIntersect){
					res = tres;
				}
				isFirstIntersect = false;
			}
			
			return res;
		}
	}

}
