package models;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Hashtable;

import com.hp.hpl.jena.query.QueryExecution;
import com.hp.hpl.jena.query.QueryExecutionFactory;
import com.hp.hpl.jena.query.QueryFactory;
import com.hp.hpl.jena.query.QuerySolution;
import com.hp.hpl.jena.query.ResultSet;
import com.hp.hpl.jena.rdf.model.Model;
import com.hp.hpl.jena.rdf.model.Resource;
import com.hp.hpl.jena.vocabulary.RDF;
import com.hp.hpl.jena.vocabulary.RDFS;

public class Utils {
	
	private static Hashtable<String, Hashtable<String, String>> mappingClassSynonymsAndProperties;
	static {
		mappingClassSynonymsAndProperties = new Hashtable<String, Hashtable<String, String>>();
		Hashtable<String, String> synonymsAndProperties = new Hashtable<String, String>();
		synonymsAndProperties.put("hascountry", "hasCountry");
		synonymsAndProperties.put("hastvseriestitle", "hasTVSeriesTitle");
		mappingClassSynonymsAndProperties.put("TVSeries", synonymsAndProperties);
	}
	
	
	private static Hashtable<String, String> mappingClassNameProperty;
	static{
		mappingClassNameProperty = new Hashtable<String,String>();
		mappingClassNameProperty.put("TVSeries", "hasTVSeriesTitle");
		mappingClassNameProperty.put("Actor", "hasName");
		mappingClassNameProperty.put("Char", "hasName");
		mappingClassNameProperty.put("Creator", "hasName");
		mappingClassNameProperty.put("Character", "hasName");
	}
	
	private static Hashtable<String, String> mappingURLClass;
	static{
		mappingURLClass = new Hashtable<String,String>();
		mappingURLClass.put("TVSeries", "title");
		mappingURLClass.put("Actor", "name");
		mappingURLClass.put("Char", "character");
		mappingURLClass.put("Creator", "name");
		mappingURLClass.put("Character", "character");
	}
	
	private static Hashtable<String, ArrayList<String>> mappingClassClassConnectingProperty;
	static{
		mappingClassClassConnectingProperty = new Hashtable<String,ArrayList<String>>();
		ArrayList<String> auxA = new ArrayList<String>();
		auxA.add("hasTVSeriesActor");
		auxA.add("hasStar");
		mappingClassClassConnectingProperty.put("TVSeriesActor", auxA);
		
		auxA = new ArrayList<String>();
		auxA.add("hasPerformedInTVSeries");
		auxA.add("isStarOf");
		mappingClassClassConnectingProperty.put("ActorTVSeries", auxA);
		
		auxA = new ArrayList<String>();
		auxA.add("isCreatedByCreator");
		mappingClassClassConnectingProperty.put("TVSeriesCreator", auxA);
		
		auxA = new ArrayList<String>();
		auxA.add("hasCreatedShow");
		mappingClassClassConnectingProperty.put("CreatorTVSeries", auxA);
	}
	
	
	
	
	public static Hashtable<String, Hashtable<String, String>> getMappingClassSynonymsAndProperties() {
		return mappingClassSynonymsAndProperties;
	}

	public static void setMappingClassSynonymsAndProperties(
			Hashtable<String, Hashtable<String, String>> mappingClassSynonymsAndProperties) {
		Utils.mappingClassSynonymsAndProperties = mappingClassSynonymsAndProperties;
	}

	public static Hashtable<String, ArrayList<String>> getMappingClassClassConnectingProperty() {
		return mappingClassClassConnectingProperty;
	}

	public static void setMappingClassClassConnectingProperty(
			Hashtable<String, ArrayList<String>> mappingClassClassConnectingProperty) {
		Utils.mappingClassClassConnectingProperty = mappingClassClassConnectingProperty;
	}

	public static Hashtable<String, String> getMappingURLClass() {
		return mappingURLClass;
	}

	public static void setMappingURLClass(Hashtable<String, String> mappingURLClass) {
		Utils.mappingURLClass = mappingURLClass;
	}

	public static Hashtable<String, String> getMappingClassNameProperty() {
		return mappingClassNameProperty;
	}

	public static void setMappingClassNameProperty(
			Hashtable<String, String> mappingClassNameProperty) {
		Utils.mappingClassNameProperty = mappingClassNameProperty;
	}

	public static ArrayList<String> getAllGenres(){
		Model model = TripleStoreManager.getInstance();
		ArrayList<String> res = new ArrayList<String>();
		
		//StmtIterator iter =model.getResource(TSModel.PREFIX+"Genre").listProperties(RDF)
		//rdfs:subClassOf
		//System.out.println("RDFS.subClassOf "+RDFS.subClassOf);
		String q1 = "PREFIX foaf:   <"+TSModel.PREFIX+"> PREFIX foaf2: <"+TSModel.SUBCLASSPREFIX+"> SELECT ?s WHERE { ?s foaf2:subClassOf foaf:Genre}";
		com.hp.hpl.jena.query.Query query = QueryFactory.create(q1);
		QueryExecution qexec = QueryExecutionFactory.create(query, model);
		ResultSet results = qexec.execSelect();
		while(results.hasNext()){
			QuerySolution qs=results.next();
			
			Resource genreResource = qs.getResource("?s");
			res.add(genreResource.toString().replace(TSModel.PREFIX, ""));
		}
		return res;
	}

	public static ArrayList<String> getAllYears(){
		Model model = TripleStoreManager.getInstance();
		ArrayList<String> res = new ArrayList<String>();

		Hashtable<Integer, Integer> hSAux = new Hashtable<Integer, Integer>();
		String q1 = "PREFIX foaf:   <"+TSModel.PREFIX+"> SELECT ?o WHERE { ?s foaf:hasYear ?o}";
		com.hp.hpl.jena.query.Query query = QueryFactory.create(q1);
		QueryExecution qexec = QueryExecutionFactory.create(query, model);
		ResultSet results = qexec.execSelect();
		while(results.hasNext()){
			QuerySolution qs=results.next();
			String year = qs.getLiteral("?o").toString();
			
			if(year.equals("NOTFOUND")) continue;
			int yearValue = qs.getLiteral("?o").getInt();
			hSAux.put(yearValue, 1); //!!!

		}
		Integer[] aI = new Integer[hSAux.keySet().size()];
		hSAux.keySet().toArray(aI);
		Arrays.sort(aI);
		for(int i=0; i<aI.length; i++){
			//System.out.println("year : "+aI[i]);
			res.add(""+aI[i]);
		}
		return res;
	}
	
	public static ArrayList<String> getAllTags(){
		Model model = TripleStoreManager.getInstance();
		ArrayList<String> res = new ArrayList<String>();
		String q1 = "PREFIX foaf:   <"+TSModel.PREFIX+"> PREFIX foaf2: <"+TSModel.TYPEPREFIX+">  SELECT ?o WHERE { ?s foaf2:type foaf:Tag . ?s foaf:hasTagName ?o}";
		com.hp.hpl.jena.query.Query query = QueryFactory.create(q1);
		QueryExecution qexec = QueryExecutionFactory.create(query, model);
		ResultSet results = qexec.execSelect();
		while(results.hasNext()){
			QuerySolution qs=results.next();
			String tagName = qs.getLiteral("?o").toString();
			//System.out.println("tag name: "+tagName);
			res.add(tagName);

		}

		return res;
	}
	
	public static Person getActorFromChar(String tvCharID){
		Model model = TripleStoreManager.getInstance();
		String q1 = "PREFIX foaf:   <"+TSModel.PREFIX+"> SELECT ?o WHERE { foaf:"+tvCharID+" foaf:isInterpretedBy ?o}";
		com.hp.hpl.jena.query.Query query = QueryFactory.create(q1);
		QueryExecution qexec = QueryExecutionFactory.create(query, model);
		ResultSet results = qexec.execSelect();
		if(results.hasNext()){
			QuerySolution qs=results.next();
			
			Resource actorResource = qs.getResource("?o");
			return Person.get(actorResource.toString().replace(TSModel.PREFIX, "")); 
		}
		return null;
		
	}
}
