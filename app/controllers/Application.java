package controllers;

import play.*;
import play.mvc.*;

import java.util.*;

import com.hp.hpl.jena.query.QueryExecution;
import com.hp.hpl.jena.query.QueryExecutionFactory;
import com.hp.hpl.jena.query.QuerySolution;
import com.hp.hpl.jena.query.ResultSet;

import models.*;

public class Application extends Controller {

    public static void index() {
    	ArrayList<Pair<String, String>> allSeries = Series.getAll(Series.getAllSF_NAME);
    	ArrayList<Pair<String, String>> allActors = Person.getAll(Person.ACTOR, Person.getAllSF_NAME);
    	ArrayList<Pair<String, String>> allCreators = Person.getAll(Person.CREATOR, Person.getAllSF_NAME);
    	ArrayList<String> allGeneres =  Utils.getAllGenres();
    	ArrayList<String> allYears = Utils.getAllYears();
    	ArrayList<String> allTags = Utils.getAllTags();
    	ArrayList<Series> recomended = Recomendations.getThem(session.getId(), 4);
    	
    	Collections.sort(allSeries);
    	Collections.sort(allActors);
    	Collections.sort(allCreators);
    	Collections.sort(allTags);
    	Collections.sort(allYears);
    	Collections.sort(allGeneres);
    	
    	Collections.reverse(allYears);
    	
        render(allSeries, allActors, allCreators, allGeneres, allYears, allTags, recomended);
    }
    

    public static void name(String id) {
    	if(id!=null){
    		Person person = Person.get(id);
    		ArrayList<Series> recomended = Recomendations.getThem(session.getId());
    		
    		render(person, recomended);
    	}
	}
    
    public static void title(String id) {
    	if(id!=null){
	    	Series series = Series.get(id);
	    	ArrayList<Pair<Person, Person>> actor2Char = new ArrayList<Pair<Person,Person>>();
	    	for(Person ch : series.getChars()){
	    		Person ac = Utils.getActorFromChar(ch.getId());
	    		if(ac != null){
	    			actor2Char.add(new Pair<Person, Person>(ac, ch));
	    		}
	    		else{
	    			Logger.error("Found no correspondence to a char using Utils.getActorFromChar.");
	    		}
	    	}
	    	
	    	ArrayList<Hashtable<String, String>> sr = series.getReviews();
	    	for(int i=0; i < sr.size(); i++){
	    		Hashtable<String, String> tr = sr.get(i);
	    		if(tr.containsKey("TextReview") && tr.get("TextReview").indexOf("*** This review may contain spoilers ***") >= 0){
	    			sr.remove(i);
	    			i--;
	    		}
	    	}
	    	
	    	series.setNumberOfReviews(sr.size()+"");
	    	
	    	Recomendations.seenThis(session.getId(), series);
	    	ArrayList<Series> recomended = Recomendations.getThem(session.getId(), 5, id);
	    	
			render(series, actor2Char, recomended);
    	}
	}
    
    public static void testSeriesAndPerson(){
    	
    	Series series = Series.get("tt0121955");
    	/*System.out.println("Seasons: "+series.getSeriesAndEps().size());
    	for(int i = 0; i<series.getSeriesAndEps().size(); i++) {
    		for(int j = 0; j<series.getSeriesAndEps().get(i).size(); j++) {
        		Logger.info("episode title "+i+ " "+j+": "+series.getSeriesAndEps().get(i).get(j).get("EpisodeTitle"));
        		
        	}
    		
    	}
    	
    	for(int i = 0; i<series.getReviews().size(); i++) {
    		Logger.info("review "+i+": "+series.getReviews().get(i).get("ReviewTitle"));
    		Logger.info("review local "+i+": "+series.getReviews().get(i).get("Localization"));
    		Logger.info("review author "+i+": "+series.getReviews().get(i).get("WrittenBy"));
    		Logger.info("review author id"+i+": "+series.getReviews().get(i).get("WrittenByID"));
    		Logger.info("review people found usefull"+i+": "+series.getReviews().get(i).get("PeopleFoundUsefull"));
    		Logger.info("rated: "+series.getAge_rating());
    	}
    	
    	for(int i = 0; i<series.getChars().size(); i++) {
    		Logger.info("chars "+i+": "+series.getChars().get(i).getName());
    		Logger.info("chars "+i+": "+series.getChars().get(i).getId());
    		
    	}
    	
    	for(int i = 0; i< series.getActor2Char().size(); i++){
    		Logger.info("actor2char actor: "+series.getActor2Char().get(i).t.getName()+" char: "+series.getActor2Char().get(i).k.getName());
    	}
    	
    	Utils.getAllGenres();
    	ArrayList<String> aS = Utils.getAllTags();
    	Utils.getAllYears();
    	for(int i = 0; i<aS.size(); i++){
    		Logger.info("tag name1: "+aS.get(i));
    	}
    	
    	SemanticSearch ss = new SemanticSearch("");
    	//ss.find("TVSeries", "south");
    	
    	Person p1 = Person.get("nm0999606");
    	ArrayList<Series> seriesAL = p1.getPersonSeries();
    	for(int i = 0; i<seriesAL.size(); i++){
    		Logger.info(" seriesAL "+seriesAL.get(i).getTitle());
    	}
    	
    	//ss.find("Genre", "mance");
    	//ss.find("TVSeries", "south");
    	
    	//new SemanticSearch("TVSeries south").getResult();
    	
    	System.out.println("*****************");
    	ArrayList<SearchResult> arrayLSearchResult = SemanticSearchUtils.find("TVSeries", "south", "Actor");
    	System.out.println("+++++");
    	for(SearchResult sR : arrayLSearchResult){
    		System.out.println("++ "+sR.getPageHref()+" "+sR.getPageTitle());
    	}
    	
    	System.out.println("*****************");
    	arrayLSearchResult = SemanticSearchUtils.find("Actor", "Summer Glau", "TVSeries");
    	System.out.println("+++++");
    	for(SearchResult sR : arrayLSearchResult){
    		System.out.println("++ "+sR.getPageHref()+" "+sR.getPageTitle());
    	}
    	
    	System.out.println("*****************");
    	arrayLSearchResult = SemanticSearchUtils.find("TVSeries", "South Park", "Creator");
    	System.out.println("+++++");
    	for(SearchResult sR : arrayLSearchResult){
    		System.out.println("++ "+sR.getPageHref()+" "+sR.getPageTitle());
    	}
    	System.out.println("*****************");
    	arrayLSearchResult = SemanticSearchUtils.findByProp("TVSeries", "hasTVSeriesTitle", "sou");
    	System.out.println("+++++");
    	for(SearchResult sR : arrayLSearchResult){
    		System.out.println("++-- "+sR.getPageHref()+" "+sR.getPageTitle());
    	}
    	
    	System.out.println("*****************");
    	arrayLSearchResult = SemanticSearchUtils.findByProp("Actor", "hasName", "Stewart");
    	System.out.println("+++++");
    	for(SearchResult sR : arrayLSearchResult){
    		System.out.println("++-- "+sR.getPageHref()+" "+sR.getPageTitle());
    	}*/
    	//Utils.getAllTags();
    	ArrayList<Series> goodTVSeriesArrayL = Series.getAllGoodTVSeries();
    	for(int i=0; i<goodTVSeriesArrayL.size(); i++){
    		System.out.println("goodTVSeriesArrayL "+goodTVSeriesArrayL.get(i).getTitle());
    	}
    	
    	ArrayList<Series> getAllInterestingTVSeriesA = Series.getAllInterestingTVSeries();
    	for(int i=0; i<getAllInterestingTVSeriesA.size(); i++){
    		System.out.println("getAllInterestingTVSeriesA "+getAllInterestingTVSeriesA.get(i).getTitle() + " "+getAllInterestingTVSeriesA.get(i).isInteresting());
    	}
    	ArrayList<SearchResult> aSR= SemanticSearchUtils.find("Tag", "");
    	System.out.println("tags...");
    	for(int i =0; i<aSR.size(); i++){
    		System.out.println("aSR "+ aSR.get(i).getPageTitle());
    		
    	}
    	
    	System.out.println("find By Prop");
    	ArrayList<SearchResult> aSR1= SemanticSearchUtils.findByProp("TVSeries", "hasCountry", "USA", "ing");
    	for(int i =0; i<aSR1.size(); i++){
    		System.out.println("aSR1 "+ aSR1.get(i).getPageTitle());
    		
    	}
    	
    	System.out.println("find By Prop");
    	ArrayList<SearchResult> aSR2= SemanticSearchUtils.findByProp("Actor", "hasDateOfBirth", "1972", "ing");
    	for(int i =0; i<aSR2.size(); i++){
    		System.out.println("aSR2 "+ aSR2.get(i).getPageTitle());
    		
    	}
    	ArrayList<Series> messages = new ArrayList<Series>();
    	ArrayList<String> ids = Series.getAll();
    	for(int i = 0; i<ids.size(); i++){
    		System.out.println("cena "+ids.get(i));
    		Series.get(ids.get(i));
    		
    	}
    	System.out.println("find by range");
    	//SemanticSearchUtils.findByRange();
    	
    	
    	
    	
    	System.out.println("find by rating");
    	//SemanticSearchUtils.findByRange();
    	
    	
    	
    	ArrayList<SearchResult> aSRRR= SemanticSearchUtils.findByRange("TVSeries", "hasRating", 8.9, 10);
    	System.out.println("range...");
    	for(int i =0; i<aSRRR.size(); i++){
    		System.out.println("rating :::"+ aSRRR.get(i).getPageTitle());
    		
    	}
    	
    	ArrayList<SearchResult> aSRR= SemanticSearchUtils.findByRange("TVSeries", SemanticSearchUtils.try2Resolve2Prop("TVSeries","Year", "ing"), 1990.0, 2000.0);
    	System.out.println("range...");
    	for(int i =0; i<aSRR.size(); i++){
    		System.out.println("aSRR +++"+ aSRR.get(i).getPageHref());
    		
    	}
    	
    	System.out.println("find By Prop PT");
    	ArrayList<SearchResult> aSR2pt= SemanticSearchUtils.findByPropPT(SemanticSearchUtils.try2Resolve2Class("Series"), "tituloDeSerie", "902");
    	for(int i =0; i<aSR2pt.size(); i++){
    		System.out.println("aSR2pt find by prop pt+ "+ aSR2pt.get(i).getPageTitle());
    		
    	}
    	
    	//System.out.println("paroudasdasdasdsa");
    	//Series.get("tt1641349");
    	System.out.println("find By Prop PT1");
    	ArrayList<SearchResult> aSR2pt1= SemanticSearchUtils.findByPropPT(SemanticSearchUtils.try2Resolve2Class("Series"), SemanticSearchUtils.try2Resolve2Prop("TVSeries","pais", "pt"), "Canada");
    	for(int i =0; i<aSR2pt1.size(); i++){
    		System.out.println("aSR2pt find by prop pt+++ "+ aSR2pt1.get(i).getPageTitle());
    		
    	}
    	
    	
    	System.out.println("find By Prop PT11");
    	ArrayList<SearchResult> aSR2pt11= SemanticSearchUtils.findByProp(SemanticSearchUtils.try2Resolve2Class("Series"), SemanticSearchUtils.try2Resolve2Prop("TVSeries","pais", "pt"), "Canada", "pt");
    	for(int i =0; i<aSR2pt11.size(); i++){
    		System.out.println("aSR2pt find by prop pt+++ (PT) "+ aSR2pt11.get(i).getPageTitle());
    		
    	}
    	
    	/////////////////////////////////////
    	System.out.println("*****************");
    	ArrayList<SearchResult> arrayLSearchResult = SemanticSearchUtils.find("TVSeries", "south", "Actor");
    	System.out.println("+++++");
    	for(SearchResult sR : arrayLSearchResult){
    		System.out.println("1++ "+sR.getPageHref()+" "+sR.getPageTitle());
    	}
    	
    	System.out.println("*****************");
    	arrayLSearchResult = SemanticSearchUtils.find("Actor", "Summer Glau", "TVSeries");
    	System.out.println("+++++");
    	for(SearchResult sR : arrayLSearchResult){
    		System.out.println("2++ "+sR.getPageHref()+" "+sR.getPageTitle());
    	}
    	
    	System.out.println("*****************");
    	arrayLSearchResult = SemanticSearchUtils.find("TVSeries", "South Park", "Creator");
    	System.out.println("+++++");
    	for(SearchResult sR : arrayLSearchResult){
    		System.out.println("3++ "+sR.getPageHref()+" "+sR.getPageTitle());
    	}
    	
    	
    	
    	
    	//System.out.println(Series.get("tt0060034").isInteresting());
    	/*ss.find("TVSeries", "south");
    	System.out.println("---------------------sas");
    	ss.find("Genre", "Action");
    	System.out.println("---------------------sas");
    	ss.find("Year", "2011");
    	System.out.println("-------------------- Politician");
    	ss.find("Tag", "Politician");
    	System.out.println("-------------------- Year11");
    	ss.find("Year", "2010");*/
    	/*Logger.info("name: "+series.getTitle()+" language "+series.getLanguage()+" number of seasons "+series.getNumberOfSeasons()+" number of reviews "+series.getNumberOfReviews());
    	for(int i = 0; i<series.getCreators().size(); i++) {
    		Logger.info("creator "+i+": "+series.getCreators().get(i).getName());
    		
    	}
    	
    	for(int i = 0; i<series.getActors().size(); i++) {
    		Logger.info("actors "+i+": "+series.getActors().get(i).getName());
    		
    	}
    	
    	
    	for(int i = 0; i<series.getPlotKeywords().size(); i++) {
    		Logger.info("key words "+i+": "+series.getPlotKeywords().get(i));
    		
    	}
    	

    	
    	for(int i = 0; i<series.getSeriesAndEps().size(); i++) {
    		for(int j = 0; j<series.getSeriesAndEps().get(i).size(); j++) {
        		Logger.info("episode title "+i+ " "+j+": "+series.getSeriesAndEps().get(i).get(j).get("EpisodeTitle"));
        		
        	}
    		
    	}
    	
    	series.getAll();
    	
    	Logger.info("person name: "+Person.get("ch0271134").getName());
    	ArrayList<String> allPeople = Person.getAll(Person.CHAR);
    	for(int i = 0; i< allPeople.size(); i++){
    		Logger.info("person id: "+allPeople.get(i));

    	}
    	
    	series.getAll("NAME");
    	
    	 ArrayList<Pair<String, String>> people = Person.getAll(Person.ACTOR, "NAME");
    	 for(int i = 0; i<people.size(); i++){
    		 Logger.info("people ::: "+people.get(i).k+" "+people.get(i).t);
    	 }
    	 Logger.info("person name: "+Person.get("ch0271134").getName());
    	 
    	 for(int i = 0; i<series.getGenres().size(); i++){
    		 Logger.info("genre: "+series.getGenres().get(i));
    	 }
    	 
    	 Logger.info("rated: "+series.getAge_rating());
    	 
     	for(int i = 0; i<series.getReviews().size(); i++) {
    		Logger.info("review "+i+": "+series.getReviews().get(i).get("ReviewTitle"));
    		Logger.info("review local "+i+": "+series.getReviews().get(i).get("Localization"));
    		Logger.info("review author "+i+": "+series.getReviews().get(i).get("WrittenBy"));
    		Logger.info("review author id"+i+": "+series.getReviews().get(i).get("WrittenByID"));
    		Logger.info("rated: "+series.getAge_rating());
    	}*/
    }

        

    public static void character(String id) {
    	if(id!=null){
    		Person person = Person.get(id);
    		ArrayList<Series> recomended = Recomendations.getThem(session.getId(), 5);
    		Person actor = Utils.getActorFromChar(person.getId());
    		
    		render(person, actor, recomended);
    	}
    }
    
    public static void find(String q, String type, String lang) {
    	Hashtable<String, ArrayList<SearchResult>> res;
    	
    	if(q != null && type!=null){
    		if(type.equals("semantic")){
    			if(lang==null)
    				lang = "eng";
    			
    			res = (new SemanticSearch(q, lang)).getResult();
    		}
    		else{
    			res = (new SyntacticSearch(q)).getResult();
    		}
    	}
    	else if(type==null){
    		if(lang==null)
				lang = "eng";
    		res = (new SemanticSearch(q, lang)).getResult();
    	}
    	else{
    		res = new Hashtable<String, ArrayList<SearchResult>>();
    	}
    	
    	String qlang = lang;
    	render(res, type, q, qlang);
    }
    
  //Visual navigation info back-end
    public static void vnibe(String op){
    	if(op == null || op.length() == 0){
			render("Application/ajax.html", "[]");
			return;
		}
    	
    	if(op.equals("ALLSERIESINFO")){
    		String jsonStr = JSONAssemler.toJSONMedium(Recomendations.getAllSeries());
    		render("Application/ajax.html", jsonStr);
    	}
    }
    
    //Visual navigation back-end
    public static void vnbe(String id, String query){
		if(id == null || id.length() == 0){
			render("Application/ajax.html", "[]");
			return;
		}
    	
		ArrayList<Pair<Series, Double>> res = Recomendations.orderByTarget(Series.get(id));
		
		if(query!=null){
			Hashtable<String, ArrayList<SearchResult>> qres = (new SemanticSearch(query, "eng")).getResult();
			if(qres.containsKey(SearchModel.SR_TITLE)){
				ArrayList<SearchResult> qs = qres.get(SearchModel.SR_TITLE);
				if(qs.size()>0){
					for(int i=0; i < res.size();i++){
						String sid = res.get(i).t.getId();
						boolean contains = false;
						
						for(SearchResult sr : qs){
							if(sr.getPageHref().endsWith(sid)){
								contains = true;
								break;
							}
						}
						
						if(!contains){
							res.remove(i);
							i--;
						}
					}
				}
				else{
					res = new ArrayList<Pair<Series,Double>>();
				}
			}
			else
				res = new ArrayList<Pair<Series,Double>>();
		}
		
		String jsonStr = JSONAssemler.toJSONLite(res);
		render("Application/ajax.html", jsonStr);
    }
    
    public static void visual(String id){
    	Series target = Series.get(id != null ? id : "tt1592154");
    	String jsonStr = JSONAssemler.toJSONLite(Recomendations.orderByTarget(target));
    	
    	render(target, jsonStr);
    }
    
    public static void clearhistory(){
    	Recomendations.clearHistory(session.getId());
    	redirect("/index");
    }
}